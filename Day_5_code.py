import math
def volume_balloon(r):
    return round(((4/3)*math.pi*(r**3)))
def Distance_btw_points(point1,point2):
    return round(math.sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2+(point1[2]-point2[2])**2),4)
def first_balloon(lst):
    x=min((right_top_coordinates[0]-lst[0]),(lst[0]-left_bottom_coordinates[0]))
    y=min((right_top_coordinates[1]-lst[1]),(lst[1]-left_bottom_coordinates[1]))
    z=min((right_top_coordinates[2]-lst[2]),(lst[2]-left_bottom_coordinates[2]))
    return round(volume_balloon(min(x,y,z)))
def second_balloon(lst):
    lst2=[]
    for i in lst:
        x=min((right_top_coordinates[0]-i[0]),(i[0]-left_bottom_coordinates[0]))
        y=min((right_top_coordinates[1]-i[1]),(i[1]-left_bottom_coordinates[1]))
        z=min((right_top_coordinates[2]-i[1]),(i[2]-left_bottom_coordinates[2]))
        lst2.append(min(x,y,z))
    distance=Distance_btw_points(lst[0],lst[1])
    lst_volumes=[]
    for i in lst2:
        lst_volumes.append(volume_balloon(i))
        b2=0
        b1=volume_balloon(i)
        re=distance-i
        if re>0:
            s=1
            if i==1:
                s=0
            b2=volume_balloon(min(lst2[s],re))
            lst_volumes.append(b1+b2)
    return max(lst_volumes)
n=int(input())
global left_bottom_coordinates
left_bottom_coordinates=list(map(int,input().split()))
global right_top_coordinates
right_top_coordinates=list(map(int,input().split()))
total_volume=1;
for i in range(3):
    total_volume*=abs(left_bottom_coordinates[i]-right_top_coordinates[i])
list_coordinates=[]
for i in range(n):
    list_coordinates.append(tuple(map(int,input().split())))
non_zero=int(input())
if n==1:
    print(total_volume-first_balloon(list_coordinates))
elif n==2:
    
    print(total_volume-second_balloon(list_coordinates))

